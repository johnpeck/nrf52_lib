#!/bin/bash
# Name: nrfprog.sh
#
# Flashes a device
# 
# Usage: nrfprog.sh <hex file>

set -e # bash should exit the script if any statement returns a non-true 
       #return value
usage="$(basename "$0") [-h] Flash a device

"

# Default values
scale=1

while getopts ':hs:' option; do
    case "$option" in
	h) echo "$usage"
	    exit
	    ;;
	s) scale=$OPTARG
	    ;;
	:) printf "missing argument for -%s\n" "$OPTARG" >&2
	    echo "$usage" >&2
	    exit 1
	    ;;
	\?) printf "illegal option: -%s\n" "$OPTARG" >&2
	    echo "$usage" >&2
	    exit 1
	    ;;
    esac
done
shift $((OPTIND-1))

./nrfjprog --erasepage 0x0-0x80000 -f nrf52
./nrfjprog --program "$1" -f NRF52
./nrfjprog --reset -f NRF52


